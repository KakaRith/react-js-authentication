import React, { useEffect, useState } from 'react'
import { fetchData, searchData } from '../requestAPI.js/requestAPI';
import Categories from '../components/Categories';
import Pagination from '../components/Pagination';
import Backbtn from '../components/Backbtn';
import Productcard from '../components/Productcard';


export const Product = () => {
  const [categories ,setCategories]  = useState(false)
  const[products ,setProducts ] = useState([])
  const[ searchText ,setSearchtext] = useState('')
  const[ searchedProducts , setSearchedProducts] = useState([])
  const[ skip ,setSkip] = useState(0)
  const[ isSearch,setisSearch] = useState(false)
  const[ category , setCategory] = useState([])
  const[categoryproduct,setCategoryproduct] = useState([])

  useEffect(() => {
    fetchData(`https://dummyjson.com/products?limit=12&skip=${skip}`)
    .then(data => {
      setProducts(data.products)
    })
    .catch(error => {
      console.error('Error', error.message);
    });
  }, [skip]);

  useEffect(() => {
    fetchData('https://dummyjson.com/products/categories')
      .then(data => {
        setCategory(data)
      })
      .catch(error => {
        console.error('Error', error.message);
      });
  }, []);

  //categoryproduct
  const handleClick = (item) => {
    setisSearch(false)
    fetchData(`https://dummyjson.com/products/category/${item}`)
    .then(data => {
      setCategories(true)
      setCategoryproduct(data.products)
    })
    .catch(error => {
      console.error('Error', error.message);
    });
    window.scrollTo(0,120)
}

  //pagination
  const handlePaginationClick =(number)=>{
    setSkip((number * 12)-12)
    window.scrollTo(0,75)
}

  //search
  const onSearch = (event) => {
    event.preventDefault()
      searchData(`https://dummyjson.com/products/search?q=${searchText}`)
      .then(data => {
        setSearchedProducts(data)
        setisSearch(true)
      })
      .catch(error => {
        console.error('Error', error.message);
      });
    
  }
  //back button
  const back =()=>{
    setisSearch(false)
  }
  //All button
  const All =()=>{
    setCategories(false)
    setisSearch(false)
  }

  return (
    <div>
          <div className='col-sm-12 d-flex justify-content-center mt-5'>
            <div style={{width:'500px'}}>
             <form className="d-flex" role="search">
             <input
              onChange={(e)=>setSearchtext(e.target.value)}
               className="form-control m-0 me-2 searchinputfield" type="search" placeholder="Search"   aria-label="Search" />
            <button className="btn btn-outline-success" onClick={onSearch} >Search</button>
            </form>
            </div>
          </div>
        <div className='my-3 border-bottom border-top container  d-flex  flex-wrap'>
         <h3 className='ms-5'>Categories</h3>
         <div className='d-flex container category_container'>
         <p className='categorybtn me-2' onClick={() =>All()}>All</p> {
          category.map((item)=>{
            return  <p key={item} className='categorybtn me-2' onClick={() =>handleClick(item)}>{item}</p>
          })
         }</div>
         
         </div>
          {categories && isSearch===false?
           <div className="container-fluid">
            <div className='row'>
            {categoryproduct.map((item)=>{
            return <Categories key={item.id} item={item}/>  
          }
          )}
           </div>
          </div>
          :<div className="container-fluid">
                 <div className="row">
                   {isSearch===false?
                     (products.map((product)=>{
                       return  <Productcard key={product.id} product={product}/>
                     })) :
                     (searchedProducts.products.length === 0 ?
                       <div className='my-5'>
                         <h2 className='text-center my-5'>There is no {searchText} here</h2>
                         <Backbtn onClick={back}/>
                       </div>
                       :
                       ( searchedProducts.products.map((product)=>{
                         return  <Productcard product={product}/>
                       }))
                     )
                   }
                     {!isSearch? <Pagination  onClick={handlePaginationClick}/>:null}
                     </div>
                 </div>
          }
      </div>
  )
}
