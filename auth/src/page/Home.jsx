import React from 'react'
import img from '../assets/img/img1.png'
import Location from '../components/Location'

export const Home = () => {
  return (<>
    <div className='container-fluid p-5'>
      <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="carousel">
  <div className="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div className="carousel-inner">
    <div className="carousel-item active">
      <div>
        <img className='p-5 rounded d-block w-100' src={img}  alt="..."/>
      </div>
    </div>
    <div className="carousel-item">
      <div>
        <img className='p-5 rounded d-block w-100' src={img}  alt="..."/>
      </div>
    </div>
    <div className="carousel-item">
      <div>
        <img className='p-5 rounded d-block w-100' src={img} alt="..."/>
      </div>
    </div>
  </div>
  <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Previous</span>
  </button>
  <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Next</span>
  </button>
</div>
  </div>

  <Location/>
  </>
  )
}
