import './App.css';
import { Routes, Route, Navigate } from 'react-router-dom';
import { Home } from './page/Home';
import AuthContext from './Auth/AuthProvider';
import { useContext } from 'react';
import { Login } from './components/Login';
import { Layout } from './Layout/Layout'
import { Product } from './page/Product'
import { Productdetail } from './components/Productdetail';
import Profile from './page/Profile';
import Cart from './components/Cart';
import Checkout from './components/Checkout';

function App() {
  const { isAuthenticated } = useContext(AuthContext)

  return (
    <>
      <Routes>
        {isAuthenticated ?
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="profile" element={<Profile />} />
            <Route path='product' element={<Product />} />
            <Route path="product/:id" element={<Productdetail />} />
            <Route path="product/cart" element={<Cart/>} />
            <Route path="product/checkout" element={<Checkout/>} />
            <Route path="*" element={<Navigate to="/" />}></Route>
          </Route>
          :
          <>
            <Route index path="auth/login" element={<Login />} />
            <Route path="*" element={<Navigate to="/auth/login" />}></Route>
          </>
        }
      </Routes>
    </>
  );
}

export default App;
