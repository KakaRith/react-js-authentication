import { createSlice } from '@reduxjs/toolkit';
const sumALl = (prev , next) =>{
    return prev + next
}

const initialState = {
    cartItem: [],   // item data store in this prop
    cartTotalItem: 0, // num on card
    cartTotalPrice : 0
};


const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: { 
        addtoCart(state, action) {
            const itemIndex = state.cartItem.findIndex(item => item.id === action.payload.id);

            if (itemIndex >= 0) {
                state.cartItem[itemIndex].cartTotalQuantity += 1;
            } else {
                const tempProduct = { ...action.payload, cartTotalQuantity: 1 };
                state.cartItem.push(tempProduct); 
            }
            state.cartTotalItem = state.cartItem.length;
            state.cartTotalPrice = state.cartItem.reduce((sum, item) => sum + Math.floor((item.price * (100 - Math.ceil(item.discountPercentage))) / 100) * item.cartTotalQuantity, 0)
        },
        singleItemQty(state, action) {    // quantity of each item
            const itemIndex = state.cartItem.findIndex(item => item.id === action.payload.id);
            const updatedCartItem = [...state.cartItem];

            switch (action.payload.cal) {
                case 'add':
                    updatedCartItem[itemIndex] = {
                        ...updatedCartItem[itemIndex],
                        cartTotalQuantity: updatedCartItem[itemIndex].cartTotalQuantity + 1
                    };
                    break;

                case 'sub':
                    updatedCartItem[itemIndex] = {
                        ...updatedCartItem[itemIndex],
                        cartTotalQuantity: Math.max(0, updatedCartItem[itemIndex].cartTotalQuantity - 1)
                    };
                    break;

                default:
                    return state;
            }
            

            return {
                ...state,
                cartItem: updatedCartItem,
                cartTotalPrice: updatedCartItem.reduce((sum, item) => sum + Math.floor((item.price * (100 - Math.ceil(item.discountPercentage))) / 100) * item.cartTotalQuantity, 0)
        }},
        removeItem(state, action) {
            const updatedCartItem = state.cartItem.filter(item => item.id !== action.payload.id);
            return {
                ...state,
                cartItem: updatedCartItem,
                cartTotalItem: updatedCartItem.length,
                cartTotalPrice: updatedCartItem.reduce((sum, item) => sum + Math.floor((item.price * (100 - Math.ceil(item.discountPercentage))) / 100) * item.cartTotalQuantity, 0)
            };
        },
    }
});

export const { addtoCart, singleItemQty, removeItem } = cartSlice.actions;
export default cartSlice.reducer;
