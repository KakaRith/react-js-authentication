const locationData = [{
    Id : 1,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/phnom-penh.jpg?width=400&height=400',
    name : 'Phnom Penh',

},{
    Id : 2,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Takeo.jpg?width=400&height=400',
    name : 'Takeo',

},{
    Id : 3,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Siem_Reap_City.jpg?width=400&height=400',
    name : 'Siem Reap',

},{
    Id : 4,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Banteay_Meanchey.jpg?width=400&height=400',
    name : 'Banteay Meanchey',

},
{
    Id : 5,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/Battambang.jpg?width=400&height=400',
    name : 'Battambang',

},
{
    Id : 6,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Kampong_Cham.jpg?width=400&height=400',
    name : 'Kampong Cham',

},
{
    Id : 7,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Kampong_Chhnang.jpg?width=400&height=400',
    name : 'Kampong Chhnang',

},
{
    Id : 8,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Kampong_Speu.jpg?width=400&height=400',
    name : 'Kampong Speu',

},
{
    Id : 9,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Kampong_Thom.jpg?width=400&height=400',
    name : 'Kampong Thom',

},
{
    Id : 10,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Kampot.jpg?width=400&height=400',
    name : 'Kampot',

},
{
    Id : 11,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Kampong_Speu.jpg?width=400&height=400',
    name : 'Kampong Speu',

},  
{
    Id : 12,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Poi%20Pet.jpg?width=400&height=400',
    name : 'Poi Pet',

},
{
    Id : 13,
    Image : 'https://asia-public.foodpanda.com/marketing/production/kh/images/nl/CityTiles/pursat.jpg',
    name : 'Pursat',

},
{
    Id : 14,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Sihanoukville.jpg?width=400&height=400',
    name : 'Sihanoukville',

},
{
    Id : 15,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Svay_Rieng.jpg?width=400&height=400',
    name : 'Svay Rieng',

},
{
    Id : 16,
    Image : 'https://images.deliveryhero.io/image/fd-kh/city-tile/city-tile-Ta_Khmau.jpg?width=400&height=400',
    name : 'Ta Khmau',

},
]

export {locationData};