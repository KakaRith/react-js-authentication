import { Outlet } from 'react-router-dom'
import { removeToken } from '../Auth/token'
import { Navbar } from '../components/Navbar'
import { useContext } from 'react'
import AuthContext from '../Auth/AuthProvider'
import { Footer } from '../components/Footer'

export const Layout = () => {
  const { setUser } = useContext(AuthContext)

  function logout() { 
     removeToken();
     setUser({})
     window.location.reload();
  }
  
  return (
    <div >
      <div className='stickynav'>
       <Navbar logout={logout} />
      </div>
       
       <Outlet/>
       <div className='footer'>
       <Footer/>
       </div>
    </div>
  )
}
