import React, {  useContext, useState } from 'react'
import '../assets/css/signin.css'
import axios from 'axios';
import { saveToken } from '../Auth/token';
import AuthContext from '../Auth/AuthProvider';

export const Login = () => {
  //Gobal state 
  const {  setAuthenticated } = useContext(AuthContext)

  //state 
  const [username, setUsername] = useState('')
  const [password,setPassword] = useState('')
  const [error , setError ] = useState('')

  //user Input
  const loginData = {
    username:username ,
    password: password,
    
  };

    // handle submit
  const handleSubmit = (e) => {
    e.preventDefault();
    axios.post('https://dummyjson.com/auth/login', loginData, {
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        saveToken(response.data.token)
        setAuthenticated(true)
      })
      .catch(error => {
        if (error.response && error.response.status === 400) {
          // Handle 400 Bad Request error
          console.error('Bad Request:', error.response.data);
          setError(error.response.data.message)
        } else {
          // Handle other errors
          console.error('Error:', error.message);
        }
      });

  }
  
  return (
    <>
    <div className='container formContainer'>
      <div className='d-flex flex-column'>
        <form className='loginborder rounded p-5'>
          <h1 className='text-center' style={{color : '#1c738c'}}>Log In</h1>
          <input className='my-3' type="text" placeholder='username' 
          onChange={(e)=> setUsername(e.target.value)}
          /><br />
          <input className='my-3' type="password" placeholder='password' 
          onChange={(e)=> setPassword(e.target.value)} /><br />
          <div className='d-flex justify-content-center'>
           <button className='my-3 px-3 rounded ' style={{background : '#1c738c', color:'white'}} onClick={handleSubmit}>Log in</button>
          </div>
          <div className="text-center text-danger">{error}</div>
        </form>
      </div>
    </div>
    
    </>
  )
}
