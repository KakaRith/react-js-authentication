import React from 'react'
import { Link } from 'react-router-dom'

const Productcard = ({product}) => {
  return (
    <>
    <div key={product.id} className='col-lg-3 col-md-4 col-sm-6  my-5'>
                       <Link className="linktoproduct" to={`${product.id} `}>
                         <div className='p-3' style={{height: '300px'}}>
                           <div className='overflow-hidden bg-white rounded-3'  style={{boxShadow: '0px 0px 56px 0px rgba(0,0,0,0.34)'}}>
                           <div className='d-flex justify-content-center '>
                             <img className='rounded' height={220} src={product.thumbnail} alt="" />
                           </div>
                           <h6 className='text-center py-2'>{product.title}</h6>
                           </div>
                         </div>
                       </Link>
                       </div>
    </>
  )
}

export default Productcard