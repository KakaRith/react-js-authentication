import React from 'react'
import { locationData } from '../assets/data/location'

const Location = () => {
  return (
    <>
    <div className="container">
     <h1>Find us in these cities and many more!</h1>
      <div className="row my-5 ">
    {locationData.map((item)=>{
        return <div key={item.Id} className="col-lg-3 d-flex justify-content-center mt-4 city-img-card overflow-hidden position-relative">
          <img
             className={item.Id === 13 ? 'rounded-3 city-img wiredimg' : 'img-fluid rounded-3 city-img'}
             src={item.Image}
             alt=""
          />
          <div className='d-flex justify-content-center'>
           <p className='city-name'>{item.name}</p>
          </div>
      </div>
    })}
     </div>
    </div>

    </>
  )
}

export default Location