import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import '../assets/css/cart.css'
import { singleItemQty } from '../store/cartSlice'
import { removeItem } from '../store/cartSlice'
import { Link } from 'react-router-dom'

const Cart = () => {
    const cartItem = useSelector((state)=> state.cart.cartItem)
    const dispatch = useDispatch()
    const total = useSelector((state)=> state.cart.cartTotalPrice)

    const handleClick = (id , cal ) =>{
      let obj = {id,cal}
       dispatch(singleItemQty(obj)) 
 
    }
    const handleRemove = (item) =>{
      dispatch(removeItem(item))
    }

    return (
    <>
<div className="container mt-5">
  { cartItem.length!==0? <>
  <table className="table my-5">
    <thead>
      <tr>
       <th scope="col">#</th>
        <th scope="col">Image</th>
        <th scope='col'>Price</th>
        <th scope="col">Amount</th>
      </tr>
    </thead>
    <tbody>
      {
              cartItem.map((item , index)=> {
                return  (
                    <tr key={index}>
                    <td><i onClick={()=>handleRemove(item)} className="fa-solid fa-x text-danger border p-2"></i></td>
                    <td><img src={item.thumbnail} height="100" /></td>
                    <td>${Math.floor((item.price * (100 - Math.ceil(item.discountPercentage))) / 100) * item.cartTotalQuantity}</td>
                    <td>
                      <i onClick={() => handleClick(item.id , 'sub') }  style={{cursor:'pointer'}} className="fa-solid  fa-minus"></i>
                      <span className='text-center mx-2 text-success' style={{width:'30px'}}  >{item.cartTotalQuantity}</span> 

                    <i  onClick={() =>handleClick(item.id , 'add') } style={{cursor:'pointer'}} className="fa-solid  fa-plus"></i>
                    </td>

                  </tr>
                )
            }) 
      } 
    </tbody>
  </table> 
  <div className='d-flex border-top justify-content-end my-5 py-3' >
    <div>
      <div>
      <h4>Total :</h4>
      <p>${total}</p>
    </div>
    <Link to='/product/checkout'>
    <button className=' p-2 rounded backbtn' >
        Checkout
      </button>
    </Link>
    </div>
  </div>
  </>
  : 
  <div style={{height:'70vh'}} className='d-flex flex-column align-items-center justify-content-center'>
   <h3 className='text-center'>Empty <i class="fa-regular fs-4 fa-face-frown"></i></h3>
   <div>
   <Link to='/product'><button className=' p-1 rounded backbtn'>
      Back to Products
     </button></Link>
   </div>
  </div>
}
</div>
    </>
  )
}

export default Cart