import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import {addtoCart} from '../store/cartSlice'
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const Productdetail = () => {
  const [product, setProduct] = useState([]);
  const [loading, setLoading] = useState(true);
  const Id = useParams().id;
  const [message, setMessage] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`https://dummyjson.com/products/${Id}`);
        setProduct(response.data);
        setLoading(false); 
      } catch (error) {
        console.log(error);
      }
    };

    fetchData(Id);

    const timeoutId = setTimeout(() => {
      setMessage('o');
    }, 300);

    return () => clearTimeout(timeoutId);
  }, [Id]); 

  const handleAddtocart =() => {
    dispatch(addtoCart(product))
    toast.success('Item added to cart!', { position: 'bottom-right' });
  }

  return (
    <div className="container">
      {loading ? (
        <div className="d-flex justify-content-center py-5 my-5">
          <img
            width={150}
            src="https://i.gifer.com/origin/3f/3face8da2a6c3dcd27cb4a1aaa32c926_w200.gif"
            alt=""
          />
        </div>
      ) : (
        <div className="row my-5">
          <div className="col-lg-6 border">
            <div className="d-flex justify-content-center p-3 position-relative">
              <img
                className="rounded border img-fluid"
                height={250}
                src={product.thumbnail}
                alt=""
              />
              <p className="text-white fs-6 text-break discountPercent position-absolute">
                -{Math.ceil(product.discountPercentage)}%
              </p>
            </div>
          </div>
          <div className="col-lg-6">
            <h1>{product.title}</h1>
            <p className="pt-1 fs-1">
              $ {Math.floor((product.price * (100 - Math.ceil(product.discountPercentage))) / 100)}{' '}
              <sup className="fs-5 text-danger">
                <del>$ {product.price}</del>
              </sup>
            </p>
            <span className="fst-italic ">
              Stock remain <span className="text-success">{product.stock}</span>
            </span>
            <p className="mt-3">{product.description}</p>

            <div>
              <p onClick={()=>handleAddtocart()} className="fw-bold addtocart p-1">Add to Cart</p>
            </div>
            <ToastContainer />
          </div>
        </div>
      )}
    </div>
  );
};
