import React from 'react'
import { useSelector } from 'react-redux'

const Checkout = () => {
    const total = useSelector((state)=> state.cart.cartTotalPrice)
  return (
    <div className='d-flex justify-content-center align-items-center' style={{height:'80vh'}}>
        <h3>Your Item Total Price is : $ {total} </h3>

    </div>
  )
}

export default Checkout