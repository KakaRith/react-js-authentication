import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { fetchData } from '../requestAPI.js/requestAPI';

const Pagination = (props) => {
  const [amount, setAmount] = useState(null);


  useEffect(() => {
    fetchData(`https://dummyjson.com/products?`)
      .then(data => {
        setAmount(Math.ceil( data.total / 12));
      })
      .catch(error => {
        console.error('Error', error.message);
      });
  }, []);

  return (
    <div className='mb-5'>
      <ul className='d-flex flex-wrap'>
        {amount &&
          Array.from({ length: amount }, (_, index) => index + 1).map((num) => (
            <NavLink key={num} className='paginationss' onClick={() => props.onClick(num)}>
              {num}
            </NavLink>
          ))}
      </ul>
    </div>
  );
};

export default Pagination;
