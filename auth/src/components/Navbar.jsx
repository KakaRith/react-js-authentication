import React, { useEffect } from 'react'
import { Link, NavLink } from 'react-router-dom'
import axios from 'axios';
import { getToken } from '../Auth/token';
import { useState } from 'react';
import { useSelector } from 'react-redux';



export const Navbar = ({logout}) => {
  const [user, setUser] = useState({});
  const cartTotalItem = useSelector((state) => state.cart.cartTotalItem)

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('https://dummyjson.com/auth/me', {
          headers: {
            'Authorization': `Bearer ${getToken()}`,
          },
        });

        setUser(response.data);
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchData();
  }, []);


  return (
    <div className=''>
      <nav  style={{backgroundColor:'#eae9e6',boxShadow:'0px 0px 5px #aedbde'}} className="navbar navbar-expand-lg navbar-light ">
      <div className="container-fluid">
        <a className="navbar-brand" href="/" style={{color:'#1c738c'}}>E-Commerce</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
         <span className="navbar-toggler-icon"></span>
         </button>
       <div className="collapse navbar-collapse " id="navbarNavDropdown">
      
         <div className=' w-50'>
          <div className='d-flex justify-content-end navlink'>
           <NavLink className='text-decoration-none me-3 pagelink ' to='/'>Home</NavLink>
           <NavLink className='text-decoration-none me-3 pagelink' to='/product'>Product</NavLink>
           <button  onClick={logout} style={{background:'#1c738c' , color:'white',width:'70px'}}  className='me-3 rounded border-0 logoutbtn'>Logout</button>
          </div>
        </div>
        <div className='w-50 '>
        
          <div className='d-flex justify-content-end navlink'>
           <div className='d-flex align-items-center me-3'>
           <Link className='position-relative' to='product/cart' style={{color: '#222'}}><i className="fa-solid fa-cart-shopping fs-4 cartbtn"></i>
           <p style={{top:'-5px',right:'-10px', color:'white'}} className='position-absolute text-decoration-none rounded-circle carttotal'>{cartTotalItem}</p>
           </Link>
            </div>
           <img className='rounded-circle avatarborder ' width={40} height={40} src={
            user.image} alt="" />
            
          </div>
        </div>
        </div>
  </div>
</nav>
    </div>
  )
}
