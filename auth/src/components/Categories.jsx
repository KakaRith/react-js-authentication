import React, { useContext, useState } from 'react'
import { useEffect } from 'react'
import { fetchData } from '../requestAPI.js/requestAPI';
import AuthContext from '../Auth/AuthProvider';
import { Link } from 'react-router-dom';


const Categories = ({item}) => {
  return (
  
       <div key={item.id} className='col-lg-3 col-md-4 col-sm-6  my-5'>
        <Link className="linktoproduct" to={`${item.id} `}>
         <div className='p-3' style={{height: '300px'}}>
          <div className='overflow-hidden bg-white rounded-3'  style={{boxShadow: '0px 0px 56px 0px rgba(0,0,0,0.34)'}}>
           <div className='d-flex justify-content-center '>
         <img className='rounded' height={220} src={item.thumbnail} alt="" />
        </div>
       <h6 className='text-center py-2'>{item.title}</h6>
       </div>
     </div>
    </Link>
    </div>


  )
}

export default Categories

