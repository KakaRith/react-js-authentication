import React from 'react'

const Backbtn = (prop) => {
  return (
    <div className='d-flex justify-content-center my-5'>
     <button className=' p-2 rounded backbtn' onClick={prop.onClick}>
      Back to Products
     </button>
    </div>
  )
}

export default Backbtn