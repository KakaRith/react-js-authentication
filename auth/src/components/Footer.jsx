import React from 'react'
import img from '../assets/img/We-accept-payment–for-web-footer-1.png'
export const Footer = () => {
  return (
    <div className='container'>
       
        <div className='row'>
          <div className="col-lg-4">
            <h4 className='ms-5'>We accept</h4>
            <img className='img-fluid px-5 py-3' src={img} alt="" />
          </div>
          <div className="col-lg-4">
          <h4 className='ms-5'>Follow us</h4>
          <div className='ms-5'>
          <i className="fa-brands fs-4 me-2  fa-twitter"></i>
          <i className="fa-brands fs-4 me-2  fa-instagram"></i>
          <i className="fa-brands fs-4 me-2  fa-facebook"></i> 
          </div>        
          </div>
          <div className="col-lg-4"></div>

        </div>
        <p className='text-center'>Copy right@Angkearith</p>
    </div>
  )
}
